module.exports = function(grunt) {

  /**
   * Define "sass" tasks.
   *
   */
  grunt.loadTasks(__dirname + '/../node_modules/grunt-contrib-sass/tasks');
  grunt.config('sass', {
    dev: {
      options: {
        style: 'expanded'
      },
      files: {
        '<%= config.cssPath %>screen.css': '<%= config.sassPath %>screen.scss',
        '<%= config.cssPath %>print.css': '<%= config.sassPath %>print.scss'
      }
    },
    prod: {
      options: {
        style: 'compressed'
      },
      files: {
        '<%= config.cssPath %>screen.css': '<%= config.sassPath %>screen.scss',
        '<%= config.cssPath %>print.css': '<%= config.sassPath %>print.scss'
      }
    },
  });
};
