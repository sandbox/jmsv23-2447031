<?php

function materialize_base_load_dependencies($load_dirs) {
	for ($n = 0; $n < count($load_dirs); $n++) {
		foreach (glob(dirname(__file__) . '/includes/' . $load_dirs[$n] . '/*.inc') as $file) {
			require $file;
		}
	}
}

//directories to load
$dirs = array(
	'preprocess',
	'overrides'
);
materialize_base_load_dependencies($dirs);
