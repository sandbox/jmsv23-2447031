<?php
/**
* implements hook_preprocess_region().
*/
function materialize_base_preprocess_region(&$vars) {
	if($vars['region'] == 'footer') {
		$vars['classes_array'][] = 'container';
	}
}