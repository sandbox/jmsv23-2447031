<?php
/**
* Implements hook_preprocess_page()
*/
function materialize_base_preprocess_page(&$vars) {

	//add the proper clases depends of the active regions
	if($vars['page']['left_sidebar'] && $vars['page']['right_sidebar']) {
		$vars['left_classes'] = 's12 m6 l3';
		$vars['content_classes'] = 's12 m6 l6';
		$vars['right_classes'] = 's12 m12 l3';
	} elseif($vars['page']['left_sidebar'] && !$vars['page']['right_sidebar']) {
		$vars['left_classes'] = 's12 m6 l3';
		$vars['content_classes'] = 's12 m6 l9';
	} elseif(!$vars['page']['left_sidebar'] && $vars['page']['right_sidebar']) {
		$vars['content_classes'] = 's12 m6 l9';
		$vars['right_classes'] = 's12 m6 l3';
	} else {
		$vars['content_classes'] = 's12 m12 l12';
	}

}